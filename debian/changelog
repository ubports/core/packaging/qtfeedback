qtfeedback-opensource-src (5.0~git20180329.a14bd0bb-4~ubports20.04.1) focal; urgency=medium

  * Merge from Debian unstable.
  * debian/control:
    + Downgrade DH compat level to version 12 again.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 18 Jul 2022 22:19:39 +0200

qtfeedback-opensource-src (5.0~git20180903.a14bd0b-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 12 to 13.
    + Drop check for DEB_BUILD_OPTIONS containing "nocheck", since debhelper
      now does this.
    + debian/rules: Drop --fail-missing argument to dh_missing, which is now
      the default.

  [ Mike Gabriel ]
  * debian/:
    + Re-add qtfeedback5-doc (formerly removed by UBports package maintainers).
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 18 Jul 2022 22:02:24 +0200

qtfeedback-opensource-src (5.0~git20180903.a14bd0b-3) unstable; urgency=medium

  * debian/rules:
    + Remove broken CMake file (maybe needs later investigation in more depth).
      (Closes: #1004705).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 31 Jan 2022 22:50:50 +0100

qtfeedback-opensource-src (5.0~git20180903.a14bd0b-2) unstable; urgency=medium

  * debian/rules:
    + Don't build and install API doc files with qtdoc if the architecture does
      not ship qt5-qtdoc. (Closes: #1002972).

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 02 Jan 2022 22:21:55 +0100

qtfeedback-opensource-src (5.0~git20180903.a14bd0b-1) unstable; urgency=medium

  * Re-create upstream Git snapshot (this time created with uscan rather than
    the self-scripted get-orig-source target in d/rules).
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.
  * debian/{rules,watch}:
    + Use uscan to obtain Git snapshot. Drop self-scripted (thanks to @onlyjob)
      get-orig-source target.
  * debian/copyright:
    + Update copyright attributions.
  * debian/libqt5feedback5.symbols:
    + Update symbols.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 25 Dec 2021 22:26:06 +0100

qtfeedback-opensource-src (5.0~git20180329.a14bd0bb-3~ubports20.04.1) focal; urgency=medium

  * Import to UBports; adding UBports build controlling files
  * Revert "debian/libqt5feedback5.symbols: Update symbols for Qt 5.14."; we're
    backporting to Ubuntu 20.04 with Qt 5.12.

 -- Ratchanan Srirattanamet <ratchanan@ubports.com>  Wed, 24 Mar 2021 16:48:33 +0700

qtfeedback-opensource-src (5.0~git20180329.a14bd0bb-3) unstable; urgency=medium

  * debian/rules:
    + Typo fix. (Rename override_dh_auto_build-arch: target to
      override_dh_auto_build: target;this was part of a local test and was
      forgotten to be restored properly before last upload).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 20:18:38 +0200

qtfeedback-opensource-src (5.0~git20180329.a14bd0bb-2) unstable; urgency=medium

  * debian/rules:
    + Explictly build docs (which we don't package for now). (Closes: #968981).
  * debian/libqt5feedback5.symbols:
    + Update symbols for Qt 5.14.
  * debian/control:
    + Add B-D: qttools5-dev-tools.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 07 Sep 2020 08:36:01 +0000

qtfeedback-opensource-src (5.0~git20180329.a14bd0bb-1) unstable; urgency=medium

  [ Timo Jyrinki ]
  * Initial release. (Closes: #954035)
  * debian/patches/revert_module_version.patch:
    - Keep module version non-zero
  * debian/patches/disable_audio_playback_test.patch:
    - Skip audio playback test which may be a problem on builders

  [ Mike Gabriel ]
  * debian/control:
    + Don't build documentation packages. API documentation in QML files
      outdated.
    + Replace B-D on libqt5xcbqpa5 by libqt5gui5.
    + Drop from B-D-I: all packages, since we don't build documentation anymore
      until it has been updated.
    + Bump Standards-Version: to 4.5.0. No changes needed.
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Update Vcs-*: fields. Packaging has been moved to salsa.debian.org.
    + Add Debian UBports Team to Uploaders: field.
    + Add myself to Uploaders: field.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation. Bump to DH compat level version 12.
  * debian/{control,rules}:
    + Drop manually created dbg:pkg.
  * debian/{control,*.install}:
    + Add qtfeedback5-examples bin:pkg.
  * debian/rules:
    + Add get-orig-source target facilitating Git snapshooting.
    + Don't build docs. Drop dh_auto_build override.
    + Drop dh_auto_configure override.
    + Move --fail-missing to dh_missing override.
    + Drop now default --parallel DH option.
    + Build test binaries first, then execute tests.
    + Set all hardening build flags.
    + Stop using dpkg-architecture.
    + Honour nocheck DEB_BUILD_OPTIONS in dh_auto_test override.
    + Drop dh_builddeb override.
  * debian/copyright:
    + Update file. Detailed copyright style.
    + Add auto-generated copyright.in file.
  * debian/patches:
    + Drop fix_qt530_compilation.patch. Fixed in Git snapshot.
    + Rebase disable_audio_playback_test.patch.
    + Add disable-cmake-test.patch.
    + Rename patches to match out patch naming scheme. See README file.
  * debian/*.install:
    + Adapt to latest Git snapshot's build results.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/libqt5feedback5.symbols:
    + Add file (amd64 only for now).
  * debian/qml-module-qtfeedback.install:
    + Rename from qtdeclarative5-qtfeedback-plugin.install.
  * debian/watch:
    + Add file, pointing to the location where we may some time find release
      tarballs of qtsystems.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 16 Mar 2020 18:21:53 +0100
